from selenium import webdriver
import unittest

class URLCheckTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_links(self):
        self.browser.get('http://127.0.0.1:8000')
        assert 'Camp Care' in self.browser.title

        self.browser.get('http://127.0.0.1:8000/1')
        assert 'Camp Care' in self.browser.title

if __name__ == '__main__':
    unittest.main()
