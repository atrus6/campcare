# Generated by Django 2.1.2 on 2018-10-27 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('camper', '0005_auto_20181027_1127'),
    ]

    operations = [
        migrations.AddField(
            model_name='dosetime',
            name='display',
            field=models.CharField(default='~Time', max_length=5),
            preserve_default=False,
        ),
    ]
