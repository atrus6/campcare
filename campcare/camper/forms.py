from django import forms

from .models import PRN, Report

class PRNGiveForm(forms.Form):
    prn_choice = forms.ModelChoiceField(queryset=PRN.objects.all())
    dosage = forms.CharField()
    notes = forms.CharField(widget=forms.Textarea, required=False)
    initial = forms.CharField()

class MassDoseForm(forms.Form):
    notes = forms.CharField(widget=forms.Textarea, required=False)
    initial = forms.CharField()


class ReportGiveForm(forms.Form):
    report_choice = forms.ModelChoiceField(queryset=Report.objects.all())
    notes = forms.CharField(widget=forms.Textarea, required=False)
    initial = forms.CharField()
