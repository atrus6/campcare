from django.db import models
from django.utils import timezone
from django.utils.timezone import localtime

from dateutil.rrule import *
from dateutil.parser import *
from dateutil.tz import *
import datetime
import pytz

class Camper(models.Model):
    camper_name= models.CharField(max_length=200)
    camper_last_name = models.CharField(max_length=200)
    breeze_id = models.CharField(max_length=200)
    camper_photo = models.ImageField(upload_to='profile_photos', blank=True)
    notes = models.TextField(blank=True)
    active = models.BooleanField()

    def get_earliest_time_hours(self):
        x = Medication.objects.filter(camper=self.id)
        et = pytz.utc.localize(datetime.datetime.max)
        for med in x:
            if et > med.get_next_time():
                et = med.get_next_time()

        diff = et - timezone.now()
        return int(diff.total_seconds()//3600)

    def __str__(self):
        return self.camper_name

class Medication(models.Model):
    camper = models.ForeignKey(Camper, on_delete=models.CASCADE)
    medication_name = models.CharField(max_length=200)
    dosage = models.CharField(max_length=200, blank=True)
    number_of_pills = models.DecimalField(max_digits=3, decimal_places=1, blank=True, default=0.0)
    notes = models.TextField(blank=True)
    last_time_given = models.DateTimeField()
    times = models.CharField(max_length=250)

    def get_next_time(self):
        nt = rrulestr(self.times, dtstart=self.last_time_given)
        x = nt[0]
        return x

    def get_next_dose_hour(self):
        diff = self.get_next_time() - timezone.now()
        return int(diff.total_seconds()//3600)

    def get_hours(self):
        rr = rrulestr(self.times)

        hours = []
        for hour in rr._byhour:
            hours.append(str(localtime(datetime.datetime.combine(datetime.date.today(), datetime.time(hour, 0, 0, 0, pytz.UTC))).hour))

        return ':00, '.join(sorted(hours)) + ':00'

    def get_days(self):
        days = rrulestr(self.times)._byweekday
        if len(days) == 7:
            return 'Daily'
        names = ["Mon", 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        rv = []

        for day in days:
            rv.append(names[day])
        return ', '.join(rv)

class DoseGiven(models.Model):
    camper = models.ForeignKey(Camper, on_delete=models.SET_NULL, null=True)
    medication = models.ForeignKey(Medication, on_delete=models.SET_NULL, null=True)
    medication_name = models.CharField(max_length=200)
    time_given = models.DateTimeField(auto_now_add=True)
    dose_given = models.CharField(max_length=200, null=True)
    notes = models.TextField(blank=True)
    initials = models.CharField(max_length=10)

class PRN(models.Model):
    prn_name = models.CharField(max_length=200)

    def __str__(self):
        return self.prn_name

class Report(models.Model):
    report_name = models.CharField(max_length=200)

    def __str__(self):
        return self.report_name
