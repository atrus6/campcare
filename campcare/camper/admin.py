from django.contrib import admin

from .models import Camper, Medication
# Register your models here.

admin.site.register(Camper)
admin.site.register(Medication)
