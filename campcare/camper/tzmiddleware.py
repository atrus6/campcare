import pytz

from django.utils import timezone
from django.utils.deprecation import MiddlewareMixin

class TimezoneMiddleware(MiddlewareMixin):
    def process_request(self, request):
        tzname = 'America/New_York'
        if tzname:
            timezone.activate(pytz.timezone(tzname))
        else:
            timezone.deactivate()
