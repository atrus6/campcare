from rest_framework import serializers

from .models import Camper

class CamperSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Camper
        fields = '__all__'
