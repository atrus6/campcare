from django.urls import resolve
from django.test import TestCase

from .views import CamperListView

# Create your tests here.
class HomePageTest(TestCase):
    def test_home_resolves(self):
        found = resolve('/')
        self.assertEqual(type(found.func), type(CamperListView.as_view()))
