from django.urls import path

from . import views
from .views import CamperCreateView, CamperListView, CamperDetailView, CamperDeleteView, CamperEditView, CamperManageView
from .views import CamperListJSONView
from .views import MedicationCreateView, MedicationDeleteView, MedicationUpdateView
from .views import DoseGivenCreateView, DoseGivenListView, DoseGivenDeleteView
from .views import PRNCreateView, PRNManageView, PRNGivenView
from .views import ReportCreateView, ReportManageView, ReportGivenView
from .views import MassDoseGivenCreateView
from .views import Updates

# Views pertaining to REST API
from .views import CamperAPIView

#View pertaining to PWA
from .views import BaseLayout

urlpatterns = [
        path('', CamperListView.as_view(), name='camper-list'),
        path('add/', CamperCreateView.as_view(), name='add-camper'),
        path('<int:pk>/', CamperDetailView.as_view(), name='camper-detail'),
        path('<int:pk>/edit', CamperEditView.as_view(), name='camper-edit'),
        path('<int:pk>/add_medication/', MedicationCreateView.as_view(), name='add-medication'),
        path('<int:camper_pk>/delete_medication/<int:pk>', MedicationDeleteView.as_view(), name='delete-medication'),
        path('<int:camper_pk>/update_medication/<int:pk>', MedicationUpdateView.as_view(), name='update-medication'),
        path('<int:pk>/delete_camper', CamperDeleteView.as_view(), name='delete-camper'),
        path('<int:camper_pk>/givedose/<int:medication_pk>', DoseGivenCreateView.as_view(), name='give-dose'),
        path('<int:camper_pk>/deletedose/<int:pk>', DoseGivenDeleteView.as_view(), name='delete-dose'),
        path('<int:camper_pk>/massdose', MassDoseGivenCreateView.as_view(), name='mass-dose'),
        path('<int:pk>/logs', DoseGivenListView.as_view(), name='camper-logs'),
        path('create_prn/', views.create_prn, name='create-prn'),
        path('manage_prns/', PRNManageView.as_view(), name='manage-prn'),
        path('delete_prn/', views.delete_prn, name='delete-prn'),
        path('<int:pk>/giveprn/', PRNGivenView.as_view(), name='give-prn'),
        path('create_report/', views.create_report, name='create-report'),
        path('manage_reports/', ReportManageView.as_view(), name='manage-reports'),
        path('delete_report/', views.delete_report, name='delete-report'),
        path('<int:pk>/givereport/', ReportGivenView.as_view(), name='give-report'),
        path('manage_campers/', CamperManageView.as_view(), name='manage-campers'),
        path('manage_campers/change_active/', views.change_status, name='change-status'),
        path('json/', views.CamperListJSONView, name='json-list'),
        path('updates/', Updates.as_view(), name='updates'),

        path('api/campers/', CamperAPIView.as_view()),

        path('base_layout', BaseLayout.as_view()),

        ]
