from dateutil.rrule import *
from dateutil.parser import *

import datetime
from datetime import timedelta

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.core import serializers
from django.shortcuts import render, HttpResponse, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.timezone import localtime, now
from django.views.generic import CreateView, ListView, DetailView, DeleteView, UpdateView, TemplateView, FormView

from rest_framework import generics

from .models import Camper, Medication, DoseGiven, PRN, Report
from .forms import PRNGiveForm, MassDoseForm, ReportGiveForm
from .serializers import CamperSerializer

def toutc(hour):
    dt = datetime.time(hour)
    dt = datetime.datetime.combine(datetime.date.today(), dt) - localtime().utcoffset()
    return dt.time().hour

def workontime(data):
    times = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00',
             '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00',
             '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00',
             '21:00', '22:00', '23:00']
    day_times = [('Monday', 0), ('Tuesday', 1), ('Wednesday', 2), ('Thursday', 3), ('Friday', 4), ('Saturday', 5), ('Sunday', 6)]

    hours = []
    days = []

    for time in times:
        if time not in data:
            hours.append(toutc((int(time[:2]))))

    for day in day_times:
        if day[0] in data:
            days.append(day[1])

    return days, hours

def filtercampers(qlist, hour, search_term, active):
    if hour is not None:
        hour = toutc(int(hour))
        qlist = qlist.filter(medication__times__contains=hour)

    if search_term is not None:
        names = qlist.filter(camper_name__icontains=search_term)
        bid = qlist.filter(breeze_id=search_term)
        t = names.union(names, bid)

        qlist = qlist.intersection(t)

    if active == 'no':
        qlist = qlist.filter(active=False)
    else:
        qlist = qlist.filter(active=True)

    return qlist.order_by('camper_last_name').distinct()

class CamperListView(LoginRequiredMixin, ListView):
    model = Camper

    def get_queryset(self):
        qlist = Camper.objects.all()
        hour = self.request.GET.get('hour')
        search_term = self.request.GET.get('search_term')
        active = self.request.GET.get('active')
        return filtercampers(qlist, hour, search_term, active)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['hour'] = self.request.GET.get('hour')
        ctx['num_params'] = len(self.request.GET)
        ctx['object_list'] = sorted(ctx['object_list'], key=lambda t: t.get_earliest_time_hours())
        return ctx

@login_required
def CamperListJSONView(request):
    rv = Camper.objects.all()

    if request.GET.get('hour') is not None:
        hour = str((int(request.GET.get('hour'))+5)%24)
        rv = rv.filter(medication__times__contains=hour)

    search_term = request.GET.get('search_term')
    if search_term is not None:
        names = rv.filter(camper_name__icontains=search_term)
        bid = rv.filter(breeze_id=search_term)
        t = names.union(names, bid)

        rv = rv.intersection(t)

    if request.GET.get('active') == 'no':
       rv = rv.filter(active=False)
    else: 
        rv = rv.filter(active=True)

    print('queryset')

    return JsonResponse(serializers.serialize('json', rv.order_by('camper_last_name').distinct()), safe=False)

#def get_context_data(self, **kwargs):
#    ctx = super().get_context_data(**kwargs)
#    ctx['hour'] = self.request.GET.get('hour')
#    ctx['num_params'] = len(self.request.GET)
#    ctx['object_list'] = sorted(ctx['object_list'], key=lambda t: t.get_earliest_time_hours())
#    return ctx

class CamperDetailView(LoginRequiredMixin, DetailView):
    model = Camper

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        meds = Medication.objects.filter(camper=self.kwargs['pk'])
        ctx['meds'] = sorted(meds, key=lambda t: t.get_next_dose_hour())
        return ctx

class CamperEditView(LoginRequiredMixin, UpdateView):
    model = Camper
    fields = ('camper_name', 'breeze_id', 'camper_photo', 'notes', 'active')
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse('camper-detail', args=(self.object.id,))

    def form_valid(self, form):
        camper = form.save(commit=False)
        if 'active' in form.changed_data and camper.active:
            for med in Medication.objects.filter(camper=camper.id):
                med.last_time_given = now()
                med.save()

        camper.save()
        return super().form_valid(form)

class CamperCreateView(LoginRequiredMixin, CreateView):
    model = Camper
    fields = ('camper_name', 'breeze_id', 'camper_photo', 'notes', 'active')

    def form_valid(self, form):
        camper = form.save(commit=False)
        camper.camper_last_name = camper.camper_name.split(' ')[-1]
        camper.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('camper-detail', args=(self.object.id,))

class CamperDeleteView(LoginRequiredMixin, DeleteView):
    model = Camper
    success_url = reverse_lazy('camper-list')

class CamperManageView(LoginRequiredMixin, ListView):
    model = Camper
    template_name = 'camper/camper_manage.html'

    def get_queryset(self):
        qlist = Camper.objects.all()
        hour = self.request.GET.get('hour')
        search_term = self.request.GET.get('search_term')
        active = self.request.GET.get('active')
        return filtercampers(qlist, hour, search_term, active)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['num_params'] = len(self.request.GET)
        return ctx
    
class MedicationCreateView(LoginRequiredMixin, CreateView):
    model = Medication
    fields = ('medication_name', 'dosage', 'number_of_pills', 'notes')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        camper = get_object_or_404(Camper, pk=self.kwargs['pk'])
        ctx['camper'] = camper.camper_name
        return ctx


    def form_valid(self, form):
        medication = form.save(commit=False)
        camper = get_object_or_404(Camper, pk=self.kwargs['pk'])
        medication.camper_id = camper.pk

        days, hours = workontime(form.data)

        x = rrule(WEEKLY, byhour=hours, byweekday=days, byminute=[0,], bysecond=[0,])
        medication.times = str(x).split()[1]
        medication.last_time_given = now()
        medication.save()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('camper-detail', args=(self.kwargs['pk'],))

class MedicationUpdateView(LoginRequiredMixin, UpdateView):
    model = Medication
    fields = ('medication_name', 'dosage', 'number_of_pills', 'notes')
    template_name_suffix = '_update_form'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        camper = get_object_or_404(Camper, pk=self.kwargs['camper_pk'])
        ctx['camper'] = camper.camper_name
        return ctx

    def form_valid(self, form):
        medication = form.save(commit=False)
        camper = get_object_or_404(Camper, pk=self.kwargs['camper_pk'])
        medication.camper_id = camper.pk

        days, hours = workontime(form.data)

        x = rrule(WEEKLY, byhour=hours, byweekday=days, byminute=[0,], bysecond=[0,])
        medication.times = str(x).split()[1]
        medication.last_time_given = now()
        medication.save()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('camper-detail', args=(self.kwargs['camper_pk'],))


class MedicationDeleteView(LoginRequiredMixin, DeleteView):
    model = Medication

    def get_success_url(self):
        return reverse('camper-detail', args=(self.kwargs['camper_pk'],))

class DoseGivenDeleteView(LoginRequiredMixin, DeleteView):
    model = DoseGiven

    def get_success_url(self):
        return reverse('camper-logs', args=(self.kwargs['camper_pk'],))

class DoseGivenCreateView(LoginRequiredMixin, CreateView):
    model = DoseGiven
    fields = ('notes', 'initials')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        camper = get_object_or_404(Camper, pk=self.kwargs['camper_pk'])
        medication = get_object_or_404(Medication, pk=self.kwargs['medication_pk'])
        ctx['c_name'] = camper.camper_name
        ctx['m_name'] = medication.medication_name
        if medication.dosage != '':
            ctx['dosage'] = medication.dosage
        ctx['pill_count'] = medication.number_of_pills
        print('nop', medication.number_of_pills)

        return ctx

    def form_valid(self, form):
        dosegiven = form.save(commit=False)
        camper = get_object_or_404(Camper, pk=self.kwargs['camper_pk'])
        medication = get_object_or_404(Medication, pk=self.kwargs['medication_pk'])
        unscheduled = self.request.POST.get('unscheduled-admin', False)
        print(self.request.POST)

        if not unscheduled:
            last_time = medication.get_next_time() + timedelta(seconds=1)

            #This catches up on any "multiple missed" doses.
            next_time = max(last_time, now())
            medication.last_time_given = next_time
            medication.save()
        
        dosegiven.camper = camper
        dosegiven.medication = medication
        dosegiven.medication_name = medication.medication_name
        dosegiven.dose_given = medication.dosage
        
        dosegiven.save()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('camper-detail', args=(self.kwargs['camper_pk'],))

class MassDoseGivenCreateView(LoginRequiredMixin, FormView):
    template_name = 'camper/massdosegiven_form.html'
    form_class = MassDoseForm

    def dose(self, dose, mid, camper):
        medication = get_object_or_404(Medication, pk=mid)
        last_time = medication.get_next_time() + timedelta(seconds=1)
        next_time = max(last_time, now())
        medication.last_time_given = next_time
        dose.camper = camper
        dose.medication = medication
        dose.medication_name = medication.medication_name
        dose.dose_given = medication.dosage
        medication.save()
        dose.save()

    def form_valid(self, form):
        medication_ids = self.request.GET.getlist('mid')
        camper = get_object_or_404(Camper, pk=self.kwargs['camper_pk'])
        print('hdfhdfhdf')
        inital = self.request.POST.get('initial')
        notes = self.request.POST.get('notes')

        for mid in medication_ids:
            self.dose(DoseGiven(initials=inital, notes=notes), mid, camper)
            
        return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        meds = self.request.GET.getlist('mid')
        med_names = []
        pill_count = 0

        for med in meds:
            cm = get_object_or_404(Medication, pk=med)
            med_names.append({'name':cm.medication_name, 'dosage':cm.dosage})
            pill_count = pill_count + cm.number_of_pills

        camper = get_object_or_404(Camper, pk=self.kwargs['camper_pk'])
        ctx['c_name'] = camper.camper_name
        ctx['m_names'] = med_names
        ctx['pill_count'] = pill_count

        return ctx

    def get_success_url(self):
        return reverse('camper-detail', args=(self.kwargs['camper_pk'],))

class DoseGivenListView(LoginRequiredMixin, ListView):
    model = DoseGiven

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        camper = get_object_or_404(Camper, pk=self.kwargs['pk'])
        ctx['camper_name'] = camper.camper_name
        ctx['camper_id'] = camper.id
        return ctx

    def get_queryset(self):
        begin = self.request.GET.get('start', False)
        end = self.request.GET.get('end', False)

        if begin and end:
            begin = datetime.datetime.fromtimestamp(int(begin)//1000)
            end = datetime.datetime.fromtimestamp(int(end)//1000)
            rv = DoseGiven.objects.filter(camper=get_object_or_404(Camper, pk=self.kwargs['pk']), time_given__gte=begin, time_given__lte=end)
            return rv

        rv = DoseGiven.objects.filter(camper=get_object_or_404(Camper, pk=self.kwargs['pk']))
        return rv

class PRNCreateView(LoginRequiredMixin, CreateView):
    model = PRN
    fields = ('__all__')

    def get_success_url(self):
        return reverse('manage-prn')

class PRNManageView(LoginRequiredMixin, ListView):
    model = PRN
    template_name = 'camper/prn_manage.html'

class PRNGivenView(LoginRequiredMixin, FormView):
    template_name = 'camper/prn_given_form.html'
    form_class = PRNGiveForm

    def form_valid(self, form):
        x = DoseGiven()
        x.camper = get_object_or_404(Camper, pk=self.kwargs['pk'])
        x.medication_name = form.cleaned_data['prn_choice']
        x.dose_given = form.cleaned_data['dosage']
        x.notes = form.cleaned_data['notes']
        x.initials = form.cleaned_data['initial']
        x.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('camper-detail', args=(self.kwargs['pk'],))

class ReportCreateView(LoginRequiredMixin, CreateView):
    model = Report
    fields = ('__all__')

    def get_success_url(self):
        return reverse('manage-reports')

class ReportManageView(LoginRequiredMixin, ListView):
    model = Report
    template_name = 'camper/report_manage.html'

class ReportGivenView(LoginRequiredMixin, FormView):
    template_name = 'camper/report_given_form.html'
    form_class = ReportGiveForm

    def form_valid(self, form):
        x = DoseGiven()
        x.camper = get_object_or_404(Camper, pk=self.kwargs['pk'])
        x.medication_name = form.cleaned_data['report_choice']
        x.notes = form.cleaned_data['notes']
        x.initials = form.cleaned_data['initial']
        x.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('camper-detail', args=(self.kwargs['pk'],))

class Updates(LoginRequiredMixin, TemplateView):
    template_name = 'camper/updates.html'

@login_required
def create_prn(request):
    prn = request.GET.get('prn-name')
    print(request.GET)

    if PRN.objects.filter(prn_name__iexact=prn).exists():
        return JsonResponse({'added':False})
    else:
        np = PRN()
        np.prn_name = prn
        np.save()
        return JsonResponse({'added':True})

@login_required
def delete_prn(request):
    dprn = request.GET.get('prnid')
    PRN.objects.get(pk=int(dprn)).delete()
    return JsonResponse({'deleted':True})

@login_required
def create_report(request):
    report = request.GET.get('report-name')

    if Report.objects.filter(report_name__iexact=report).exists():
        return JsonResponse({'added':False})
    else:
        np = Report()
        np.report_name = report
        np.save()
        return JsonResponse({'added':True})

@login_required
def delete_report(request):
    dprn = request.GET.get('reportid')
    Report.objects.get(pk=int(dprn)).delete()
    return JsonResponse({'deleted':True})

@login_required
def change_status(request):
    cid = request.GET.get('camperid')
    checked = request.GET.get('checked')
    camper = get_object_or_404(Camper, pk=int(cid))
    camper.active = (checked=='true')

    if camper.active:
        for med in Medication.objects.filter(camper=camper.id):
            med.last_time_given = now()
            med.save()

    camper.save()
    return JsonResponse({'updated':True})

class CamperAPIView(generics.ListCreateAPIView):
    queryset = Camper.objects.order_by('camper_last_name')
    serializer_class = CamperSerializer

class BaseLayout(TemplateView):
    template_name ='camper/base.html'
